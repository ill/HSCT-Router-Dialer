﻿
using System;
using System.Collections.Generic;
using IniFile;

namespace DialerConfig
{
	/// <summary>
	/// Description of MyClass.
	/// </summary>
	public class DialerConfigLib
	{
		public bool firstRun = false;
		private string filePath;
		private IniFileFH iniFh;
		public string routerAddress;
		public string routerAdminUsername;
		public string routerAdminPassword;
		public bool isAutoDisconnect;
		public int autoDisconnectTime;
		public bool autoRunCalcTool;
		public string ispPassword;
		public string ispUsername;
		public bool autoSendAction;
		public DialerConfigLib(string configFilePath = "Config.ini")
		{
			filePath = configFilePath;
			if (!System.IO.File.Exists(filePath))
				firstRun = true;
			iniFh = new IniFileFH(filePath);
		}
		public void readConfig()
		{
			if (firstRun) {
				routerAddress = "192.168.1.1";
				routerAdminUsername = routerAdminPassword = "admin";
				ispUsername = ispPassword = "";
				autoRunCalcTool = isAutoDisconnect = false;
				autoDisconnectTime = 15;
				autoSendAction = true;
				saveConfig();
			}
			else
			{
				string readSection = "Router";
				routerAddress = iniFh.read(readSection, "Address");
				if (routerAddress == "") routerAddress = "192.168.1.1";
				routerAdminUsername = iniFh.read(readSection, "Username");
				if (routerAdminUsername == "") routerAdminUsername = "admin";
				routerAdminPassword = iniFh.read(readSection, "Password");
				if (routerAdminPassword == "") routerAdminPassword = "admin";
				readSection = "Isp";
				ispUsername = iniFh.read(readSection, "Account");
				ispPassword = iniFh.read(readSection, "Password");
				readSection = "AutoDisconnect";
				Boolean.TryParse(iniFh.read(readSection, "Enable"), out isAutoDisconnect);
				Int32.TryParse(iniFh.read(readSection, "Timeout"), out autoDisconnectTime);
				if (autoDisconnectTime < 1) autoDisconnectTime = 15;
				readSection = "Preference";
				Boolean.TryParse(iniFh.read(readSection, "AutoRunCalcTool"), out autoRunCalcTool);
				Boolean.TryParse(iniFh.read(readSection, "AutoSendUserAction"), out autoSendAction);
			}
		}
		public void saveConfig()
		{
			string writeSection = "Router";
			iniFh.write(writeSection, "Address", routerAddress);
			iniFh.write(writeSection, "Username", routerAdminUsername);
			iniFh.write(writeSection, "Password", routerAdminPassword);
			writeSection = "Isp";
			iniFh.write(writeSection, "Account", ispUsername);
			iniFh.write(writeSection, "Password", ispPassword);
			writeSection = "AutoDisconnect";
			iniFh.write(writeSection, "Enable", isAutoDisconnect.ToString());
			iniFh.write(writeSection, "Timeout", autoDisconnectTime.ToString());
			writeSection = "Preference";
			iniFh.write(writeSection, "AutoRunCalcTool", autoRunCalcTool.ToString());
			iniFh.write(writeSection, "AutoSendUserAction", autoSendAction.ToString());
		}
	}
}