﻿
using System;
using System.Windows.Forms;

namespace DialerMainUi
{
	public partial class HelpForm : Form
	{
		readonly Form parentForm;
		public HelpForm(Form pForm)
		{
			InitializeComponent();
			parentForm=pForm;
		}
		
		void Button1Click(object sender, EventArgs e)
		{
			Close();
		}
		
		void HelpFormFormClosed(object sender, FormClosedEventArgs e)
		{
			parentForm.Show();
		}
	}
}
