﻿
namespace DialerMainUi
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			this.buttonStart = new System.Windows.Forms.Button();
			this.buttonHelp = new System.Windows.Forms.Button();
			this.buttonExitProgram = new System.Windows.Forms.Button();
			this.labelRouterAddress = new System.Windows.Forms.Label();
			this.labelRouterAdminAcc = new System.Windows.Forms.Label();
			this.labelRouterAdminPsw = new System.Windows.Forms.Label();
			this.labelIspAcc = new System.Windows.Forms.Label();
			this.labelIspPsw = new System.Windows.Forms.Label();
			this.textBoxRouterAddress = new System.Windows.Forms.TextBox();
			this.textBoxRouterAdminAcc = new System.Windows.Forms.TextBox();
			this.textBoxRouterAdminPsw = new System.Windows.Forms.TextBox();
			this.textBoxIspAcc = new System.Windows.Forms.TextBox();
			this.textBoxIspPsw = new System.Windows.Forms.TextBox();
			this.checkBoxAutoRunCalc = new System.Windows.Forms.CheckBox();
			this.checkBoxAutoDisconnect = new System.Windows.Forms.CheckBox();
			this.checkBoxAutoUseCalc = new System.Windows.Forms.CheckBox();
			this.checkBoxRememberSethings = new System.Windows.Forms.CheckBox();
			this.labelStatusBar = new System.Windows.Forms.Label();
			this.labelAutoDissconnectMinutes = new System.Windows.Forms.Label();
			this.numericUpDownAutoDisconnectMinutes = new System.Windows.Forms.NumericUpDown();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDownAutoDisconnectMinutes)).BeginInit();
			this.SuspendLayout();
			// 
			// buttonStart
			// 
			this.buttonStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonStart.Location = new System.Drawing.Point(202, 144);
			this.buttonStart.Name = "buttonStart";
			this.buttonStart.Size = new System.Drawing.Size(75, 23);
			this.buttonStart.TabIndex = 21;
			this.buttonStart.Text = "开始拨号";
			this.buttonStart.UseVisualStyleBackColor = true;
			this.buttonStart.Click += new System.EventHandler(this.ButtonStartClick);
			// 
			// buttonHelp
			// 
			this.buttonHelp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonHelp.Location = new System.Drawing.Point(202, 173);
			this.buttonHelp.Name = "buttonHelp";
			this.buttonHelp.Size = new System.Drawing.Size(75, 23);
			this.buttonHelp.TabIndex = 22;
			this.buttonHelp.Text = "使用说明";
			this.buttonHelp.UseVisualStyleBackColor = true;
			this.buttonHelp.Click += new System.EventHandler(this.ButtonHelpClick);
			// 
			// buttonExitProgram
			// 
			this.buttonExitProgram.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonExitProgram.Location = new System.Drawing.Point(202, 202);
			this.buttonExitProgram.Name = "buttonExitProgram";
			this.buttonExitProgram.Size = new System.Drawing.Size(75, 23);
			this.buttonExitProgram.TabIndex = 23;
			this.buttonExitProgram.Text = "退出程序";
			this.buttonExitProgram.UseVisualStyleBackColor = true;
			this.buttonExitProgram.Click += new System.EventHandler(this.ButtonExitProgramClick);
			// 
			// labelRouterAddress
			// 
			this.labelRouterAddress.AutoSize = true;
			this.labelRouterAddress.Location = new System.Drawing.Point(12, 9);
			this.labelRouterAddress.Name = "labelRouterAddress";
			this.labelRouterAddress.Size = new System.Drawing.Size(65, 12);
			this.labelRouterAddress.TabIndex = 103;
			this.labelRouterAddress.Text = "路由器地址";
			// 
			// labelRouterAdminAcc
			// 
			this.labelRouterAdminAcc.AutoSize = true;
			this.labelRouterAdminAcc.Location = new System.Drawing.Point(12, 36);
			this.labelRouterAdminAcc.Name = "labelRouterAdminAcc";
			this.labelRouterAdminAcc.Size = new System.Drawing.Size(101, 12);
			this.labelRouterAdminAcc.TabIndex = 104;
			this.labelRouterAdminAcc.Text = "路由器管理员账号";
			// 
			// labelRouterAdminPsw
			// 
			this.labelRouterAdminPsw.AutoSize = true;
			this.labelRouterAdminPsw.Location = new System.Drawing.Point(12, 90);
			this.labelRouterAdminPsw.Name = "labelRouterAdminPsw";
			this.labelRouterAdminPsw.Size = new System.Drawing.Size(101, 12);
			this.labelRouterAdminPsw.TabIndex = 105;
			this.labelRouterAdminPsw.Text = "路由器管理员密码";
			// 
			// labelIspAcc
			// 
			this.labelIspAcc.AutoSize = true;
			this.labelIspAcc.Location = new System.Drawing.Point(12, 117);
			this.labelIspAcc.Name = "labelIspAcc";
			this.labelIspAcc.Size = new System.Drawing.Size(77, 12);
			this.labelIspAcc.TabIndex = 106;
			this.labelIspAcc.Text = "电信宽带账号";
			// 
			// labelIspPsw
			// 
			this.labelIspPsw.AutoSize = true;
			this.labelIspPsw.Location = new System.Drawing.Point(12, 63);
			this.labelIspPsw.Name = "labelIspPsw";
			this.labelIspPsw.Size = new System.Drawing.Size(77, 12);
			this.labelIspPsw.TabIndex = 107;
			this.labelIspPsw.Text = "电信宽带密码";
			// 
			// textBoxRouterAddress
			// 
			this.textBoxRouterAddress.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.textBoxRouterAddress.Location = new System.Drawing.Point(177, 6);
			this.textBoxRouterAddress.Name = "textBoxRouterAddress";
			this.textBoxRouterAddress.Size = new System.Drawing.Size(100, 21);
			this.textBoxRouterAddress.TabIndex = 11;
			// 
			// textBoxRouterAdminAcc
			// 
			this.textBoxRouterAdminAcc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.textBoxRouterAdminAcc.Location = new System.Drawing.Point(177, 33);
			this.textBoxRouterAdminAcc.Name = "textBoxRouterAdminAcc";
			this.textBoxRouterAdminAcc.Size = new System.Drawing.Size(100, 21);
			this.textBoxRouterAdminAcc.TabIndex = 12;
			// 
			// textBoxRouterAdminPsw
			// 
			this.textBoxRouterAdminPsw.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.textBoxRouterAdminPsw.Location = new System.Drawing.Point(177, 60);
			this.textBoxRouterAdminPsw.Name = "textBoxRouterAdminPsw";
			this.textBoxRouterAdminPsw.Size = new System.Drawing.Size(100, 21);
			this.textBoxRouterAdminPsw.TabIndex = 13;
			// 
			// textBoxIspAcc
			// 
			this.textBoxIspAcc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.textBoxIspAcc.Location = new System.Drawing.Point(177, 87);
			this.textBoxIspAcc.Name = "textBoxIspAcc";
			this.textBoxIspAcc.Size = new System.Drawing.Size(100, 21);
			this.textBoxIspAcc.TabIndex = 14;
			// 
			// textBoxIspPsw
			// 
			this.textBoxIspPsw.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.textBoxIspPsw.Location = new System.Drawing.Point(177, 114);
			this.textBoxIspPsw.Name = "textBoxIspPsw";
			this.textBoxIspPsw.Size = new System.Drawing.Size(100, 21);
			this.textBoxIspPsw.TabIndex = 15;
			// 
			// checkBoxAutoRunCalc
			// 
			this.checkBoxAutoRunCalc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.checkBoxAutoRunCalc.AutoSize = true;
			this.checkBoxAutoRunCalc.Location = new System.Drawing.Point(12, 140);
			this.checkBoxAutoRunCalc.Name = "checkBoxAutoRunCalc";
			this.checkBoxAutoRunCalc.Size = new System.Drawing.Size(108, 16);
			this.checkBoxAutoRunCalc.TabIndex = 16;
			this.checkBoxAutoRunCalc.Text = "自动启动算号器";
			this.checkBoxAutoRunCalc.UseVisualStyleBackColor = true;
			// 
			// checkBoxAutoDisconnect
			// 
			this.checkBoxAutoDisconnect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.checkBoxAutoDisconnect.AutoSize = true;
			this.checkBoxAutoDisconnect.Location = new System.Drawing.Point(12, 184);
			this.checkBoxAutoDisconnect.Name = "checkBoxAutoDisconnect";
			this.checkBoxAutoDisconnect.Size = new System.Drawing.Size(108, 16);
			this.checkBoxAutoDisconnect.TabIndex = 19;
			this.checkBoxAutoDisconnect.Text = "无使用自动断线";
			this.checkBoxAutoDisconnect.UseVisualStyleBackColor = true;
			this.checkBoxAutoDisconnect.CheckedChanged += new System.EventHandler(this.CheckBoxAutoDisconnectCheckedChanged);
			// 
			// checkBoxAutoUseCalc
			// 
			this.checkBoxAutoUseCalc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.checkBoxAutoUseCalc.AutoSize = true;
			this.checkBoxAutoUseCalc.Location = new System.Drawing.Point(12, 162);
			this.checkBoxAutoUseCalc.Name = "checkBoxAutoUseCalc";
			this.checkBoxAutoUseCalc.Size = new System.Drawing.Size(108, 16);
			this.checkBoxAutoUseCalc.TabIndex = 17;
			this.checkBoxAutoUseCalc.Text = "自动操作算号器";
			this.checkBoxAutoUseCalc.UseVisualStyleBackColor = true;
			// 
			// checkBoxRememberSethings
			// 
			this.checkBoxRememberSethings.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.checkBoxRememberSethings.AutoSize = true;
			this.checkBoxRememberSethings.Location = new System.Drawing.Point(12, 206);
			this.checkBoxRememberSethings.Name = "checkBoxRememberSethings";
			this.checkBoxRememberSethings.Size = new System.Drawing.Size(96, 16);
			this.checkBoxRememberSethings.TabIndex = 20;
			this.checkBoxRememberSethings.Text = "记住我的设置";
			this.checkBoxRememberSethings.UseVisualStyleBackColor = true;
			// 
			// labelStatusBar
			// 
			this.labelStatusBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.labelStatusBar.AutoSize = true;
			this.labelStatusBar.ForeColor = System.Drawing.SystemColors.Highlight;
			this.labelStatusBar.Location = new System.Drawing.Point(12, 225);
			this.labelStatusBar.Name = "labelStatusBar";
			this.labelStatusBar.Size = new System.Drawing.Size(77, 12);
			this.labelStatusBar.TabIndex = 117;
			this.labelStatusBar.Text = "等待用户操作";
			// 
			// labelAutoDissconnectMinutes
			// 
			this.labelAutoDissconnectMinutes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.labelAutoDissconnectMinutes.AutoSize = true;
			this.labelAutoDissconnectMinutes.Location = new System.Drawing.Point(167, 185);
			this.labelAutoDissconnectMinutes.Name = "labelAutoDissconnectMinutes";
			this.labelAutoDissconnectMinutes.Size = new System.Drawing.Size(29, 12);
			this.labelAutoDissconnectMinutes.TabIndex = 119;
			this.labelAutoDissconnectMinutes.Text = "分钟";
			// 
			// numericUpDownAutoDisconnectMinutes
			// 
			this.numericUpDownAutoDisconnectMinutes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.numericUpDownAutoDisconnectMinutes.Location = new System.Drawing.Point(125, 183);
			this.numericUpDownAutoDisconnectMinutes.Maximum = new decimal(new int[] {
			120,
			0,
			0,
			0});
			this.numericUpDownAutoDisconnectMinutes.Minimum = new decimal(new int[] {
			1,
			0,
			0,
			0});
			this.numericUpDownAutoDisconnectMinutes.Name = "numericUpDownAutoDisconnectMinutes";
			this.numericUpDownAutoDisconnectMinutes.Size = new System.Drawing.Size(36, 21);
			this.numericUpDownAutoDisconnectMinutes.TabIndex = 18;
			this.numericUpDownAutoDisconnectMinutes.Value = new decimal(new int[] {
			15,
			0,
			0,
			0});
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(289, 246);
			this.Controls.Add(this.numericUpDownAutoDisconnectMinutes);
			this.Controls.Add(this.labelAutoDissconnectMinutes);
			this.Controls.Add(this.labelStatusBar);
			this.Controls.Add(this.checkBoxRememberSethings);
			this.Controls.Add(this.checkBoxAutoUseCalc);
			this.Controls.Add(this.checkBoxAutoDisconnect);
			this.Controls.Add(this.checkBoxAutoRunCalc);
			this.Controls.Add(this.textBoxIspPsw);
			this.Controls.Add(this.textBoxIspAcc);
			this.Controls.Add(this.textBoxRouterAdminPsw);
			this.Controls.Add(this.textBoxRouterAdminAcc);
			this.Controls.Add(this.textBoxRouterAddress);
			this.Controls.Add(this.labelIspPsw);
			this.Controls.Add(this.labelIspAcc);
			this.Controls.Add(this.labelRouterAdminPsw);
			this.Controls.Add(this.labelRouterAdminAcc);
			this.Controls.Add(this.labelRouterAddress);
			this.Controls.Add(this.buttonExitProgram);
			this.Controls.Add(this.buttonHelp);
			this.Controls.Add(this.buttonStart);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MinimumSize = new System.Drawing.Size(305, 285);
			this.Name = "MainForm";
			this.Text = "电信校园路由器拨号工具";
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainFormFormClosed);
			this.Load += new System.EventHandler(this.MainFormLoad);
			((System.ComponentModel.ISupportInitialize)(this.numericUpDownAutoDisconnectMinutes)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		private System.Windows.Forms.NumericUpDown numericUpDownAutoDisconnectMinutes;
		private System.Windows.Forms.Label labelAutoDissconnectMinutes;
		private System.Windows.Forms.Label labelStatusBar;
		private System.Windows.Forms.CheckBox checkBoxRememberSethings;
		private System.Windows.Forms.CheckBox checkBoxAutoUseCalc;
		private System.Windows.Forms.CheckBox checkBoxAutoDisconnect;
		private System.Windows.Forms.CheckBox checkBoxAutoRunCalc;
		private System.Windows.Forms.TextBox textBoxIspPsw;
		private System.Windows.Forms.TextBox textBoxIspAcc;
		private System.Windows.Forms.TextBox textBoxRouterAdminPsw;
		private System.Windows.Forms.TextBox textBoxRouterAdminAcc;
		private System.Windows.Forms.TextBox textBoxRouterAddress;
		private System.Windows.Forms.Label labelIspPsw;
		private System.Windows.Forms.Label labelIspAcc;
		private System.Windows.Forms.Label labelRouterAdminPsw;
		private System.Windows.Forms.Label labelRouterAdminAcc;
		private System.Windows.Forms.Label labelRouterAddress;
		private System.Windows.Forms.Button buttonExitProgram;
		private System.Windows.Forms.Button buttonHelp;
		private System.Windows.Forms.Button buttonStart;
	}
}
