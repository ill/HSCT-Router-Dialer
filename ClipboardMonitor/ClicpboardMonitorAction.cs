﻿
using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Forms;

namespace ClipboardMonitor
{
	/// <summary>
	/// Description of MyClass.
	/// </summary>
	public class ClicpboardMonitorAction
	{
		private string clipboardOldString;
		public ClicpboardMonitorAction()
		{
			clipboardOldString = Clipboard.GetText();
		}
		public string getNewString()
		{
			while (true)
			{
				string newClipboardString = Clipboard.GetText();
				if (clipboardOldString != newClipboardString)
				{
					clipboardOldString = newClipboardString;
					return newClipboardString;
				}
				else
				{
					Thread.Sleep(500);
				}
			}
		}
		public string getNewStringStartWith(string dataStart,int timeout=0)
		{
			bool setTimeout = true;
			if (timeout == 0) setTimeout = false;
			int outOfLoopValue = timeout * 2;
			while (true)
			{
				string getedClipeboardString = getNewString();
				if (getedClipeboardString.Substring(0, dataStart.Length) == dataStart) return getedClipeboardString;
				if (setTimeout)
				{
					if (outOfLoopValue <= 0) return "";
					else outOfLoopValue -= 1;
				}
			}
		}
	}
}