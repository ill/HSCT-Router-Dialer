﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
namespace IniFile
{
	/// <summary>
	/// Description of MyClass.
	/// </summary>
	public class IniFileFH
	{
		[System.Runtime.InteropServices.DllImport("kernel32")]
		private static extern long WritePrivateProfileString(string section, string key, string val, string filePath);
		[System.Runtime.InteropServices.DllImport("kernel32")]
		private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);
		private string iniFilePath;
		public IniFileFH(string filePath)
		{
			iniFilePath = filePath;
			if (!File.Exists(iniFilePath))
			{
				File.Create(iniFilePath).Close();
			}
		}
		public void write(string section, string key, string value)
		{
			WritePrivateProfileString(section, key, value, iniFilePath);
		}
		public string read(string section,string key) {
			StringBuilder temp = new StringBuilder(255);
			GetPrivateProfileString(section, key, "", temp, 255, iniFilePath);
			return temp.ToString();
		}
	}
}